#!usr/bin/env python3

import socket, threading

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)                  #socket init 
client.connect(('127.0.0.1', 9090))                                         #connect user to server                       
writing_flag = 0                                                            #flag for controlling the writing thread

def valid_nickname():
    """Checks if nickname is an empty string"""
    while True:
        nickname = input("Choose your nickname: ")
        if nickname:
            break
        else:
            print("Nickname can't be an empty string")
    return nickname

nickname = valid_nickname()

def receive():
    """Receiving messages from the server"""
    global nickname
    global writing_flag
    
    while True:                                                 
        try:
            message = client.recv(1024).decode('ascii')
            if message == 'NICKNAME':                                       #nickname request from server
                client.send(nickname.encode('ascii'))
            elif message == 'UPDATE_NICKNAME':                              #re-request a nickname if there is a similar one
                print('This nickname is already taken')
                nickname = valid_nickname()
                client.send(nickname.encode('ascii'))
            elif message == 'LEAVE':                                        #logging out of the server
                print("Press Enter to leave")
                client.close()
                break
            elif message == 'SHUTDOWN':                                     #for cases when the server has stopped
                raise Exception
            else:
                writing_flag += 1
                print(message)
        except:                                                 
            print("An error occured! Press Enter to leave")
            client.close()
            break

        if writing_flag == 1:
            write_thread = threading.Thread(target=write)                   #start writing thread
            write_thread.start()
            writing_flag = 2
    return None
    
def write():
    """Receives a message from the user that is sent to the server"""
    while True:
        try:
            message = '{}: {}'.format(nickname, input(''))
            print('\033[1A' + '\033[K', end='')                             #ANSI escape code to remove repetition when a user enters a message
            client.send(message.encode('ascii'))
        except:
            return None

if __name__ == '__main__':
    receive_thread = threading.Thread(target=receive)
    receive_thread.start()
