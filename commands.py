def command_handle(message, client, nicknames, clients):                                #Command message structure:
    """Handles command message"""                                                       #{from_nickname}: {!command} {nickname/message} {message/''}
    message = message.decode('ascii').split(': ', 1)[1]                                 #get all content after ': '
    message_content = message.split(' ', 2)                                             #get ['command', 'nickname', message]
    command_name, message_text = message_content[0], message_content[1:]
    if command_name in command_dict:
        return command_dict[command_name](nicknames, clients, client, message_text)     #execute command
    else:
        return "Command doesn't exist.\nUse !commands_list"
        
def is_command(message):
    """Checks if the received message is a command"""
    if message[-2:] == b': ':                                                           #check for empty message
        return False
    if message.split(b': ', 1)[1][0] == ord('!'):                                       #check for command message
        return True
    else:
        return False

def on_server(nicknames, *args):
    """Show all users on server"""
    return " ".join(nicknames)

def leave(*args):
    """Disconnect user from server"""
    return "LEAVE"

def commands_list(*args):
    """Show commands list"""
    command_list = [f"!{command.__name__} - {command.__doc__}" for command in command_dict.values()]
    return "\n".join(command_list)

def to(nicknames, clients, client, message_text):
    """Sends a message to the specified user
    Parameters
    -------------
        to_nickname: str
            Nickname of the user to whom the message is sent.
        message: str
            The message sent to the user.
    -------------
    """
    if len(message_text) == 2:
        from_index = clients.index(client)
        from_nickname = nicknames[from_index]
        to_nickname = message_text[0]
        if to_nickname in nicknames:
            to_index = nicknames.index(to_nickname)
            to_client = clients[to_index]
            message = message_text[1]
            to_client.send(f"from {from_nickname}: {message}".encode('ascii'))
            client.send(f"to {to_nickname}: {message}".encode('ascii'))
        else:
            client.send("User does not exist".encode('ascii'))
    else:
        client.send("You forgot to enter a user or message".encode('ascii'))


command_dict = {
                "!commands_list": commands_list,
                "!on_server": on_server,
                "!to": to,
                "!leave": leave
                }
