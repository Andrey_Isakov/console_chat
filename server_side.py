#!usr/bin/env python3

import logging, socket, threading
import commands

host = '127.0.0.1'                                                                                  #localhost
port = 9090                                                                                         #Unreserved port

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)                                          #socket init
server.bind((host, port))                                                                           #binding host and port to socket
server.listen()

logging.basicConfig(filename='server.log',
                    format='%(levelname)s %(asctime)s %(message)s',
                    level=logging.INFO)
logging.info('----------Server started----------')

clients = []
nicknames = []

def broadcast(message):
    """Sends a message to all users"""
    for client in clients:
        client.send(message)

def handle(client):
    """Handles messages received from the user."""
    while True:
        try:                                                            
            message = client.recv(1024)
            logging.info(message.decode('ascii'))
            if commands.is_command(message):                                                        #command message check
                command_result = commands.command_handle(message, client, nicknames, clients)
                if command_result:
                    client.send(command_result.encode('ascii'))
            else:
                broadcast(message)
        except:                                                                                     #removing users                                            
            index = clients.index(client)
            clients.remove(client)
            client.close()
            nickname = nicknames[index]
            broadcast('{} left!'.format(nickname).encode('ascii'))
            nicknames.remove(nickname)
            break

def receive():
    """Receive messages from client_side"""
    try:
        while True:
            client, address = server.accept()
            print("Connected with {}".format(str(address)))
            client.send('NICKNAME'.encode('ascii'))
            nickname = client.recv(1024).decode('ascii')
            
            while nickname in nicknames:
                client.send('UPDATE_NICKNAME'.encode('ascii'))
                nickname = client.recv(1024).decode('ascii')

            nicknames.append(nickname)
            clients.append(client)
            print(f"Nickname is {nickname}")
            broadcast(f"{nickname} joined!".encode('ascii'))
            client.send('Connected to server!\n'.encode('ascii'))
            client.send('To see the available commands, enter the command --- !commands_list'.encode('ascii'))
            thread = threading.Thread(target=handle, args=(client,))
            thread.start()
    except:                                                                                         #disconnect all users when the server stops
        if clients:
            for user in clients:
                user.send('SHUTDOWN'.encode('ascii'))
        logging.warning('----------Server shutdown----------')

if __name__ == '__main__':
    receive()
